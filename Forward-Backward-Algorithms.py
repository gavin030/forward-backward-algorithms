"""
=================================================
Forward, Backward and Forward-Backward algorithms
=================================================

Author: Haotian Shi, 2016

"""



import pickle
from math import *
import sys


############################################################
# HMM algorithms
############################################################

def load_corpus(path):
    string = []
    with open(path) as f:
        for eachLine in f:
            for char in eachLine:
                if char.isalpha():
                    string.append(char)
                elif char == ' ' or char == '\n':
                    if len(string) == 0:
                        string.append(' ')
                    elif len(string) > 0 and string[-1] != ' ':
                        string.append(' ')
    return ''.join(string).lower()

def load_probabilities(path):
    with open(path) as f:
        logp_data = pickle.load(f)
    return logp_data

class HMM(object):

    def __init__(self, probabilities):
        self.logps = probabilities

    def forward(self, sequence):
        O = sequence
        T = len(sequence)
        N = len(self.logps[0])
        alpha = [dict() for t in range(T)]        
        for t in range(T):
            if t == 0:
                for i in range(1, N+1):
                    alpha[t][i] = self.logps[0][i] + self.logps[2][i][O[t]]
            else:
                for j in range(1, N+1):
                    a = max([alpha[t-1][i] + self.logps[1][i][j] for i in range(1, N+1)])
                    alpha[t][j] = a + log(sum([e ** (alpha[t-1][i] + self.logps[1][i][j] - a) for i in range(1, N+1)])) + self.logps[2][j][O[t]]
        return alpha

    def forward_probability(self, alpha):
        a = max(alpha[-1].values())
        return a + log(sum([e ** (each - a) for each in alpha[-1].values()]))

    def backward(self, sequence):
        O = sequence
        T = len(sequence)
        N = len(self.logps[0])
        beta = [dict() for t in range(T)]        
        for t in reversed(range(T)):
            if t == T-1:
                for i in range(1, N+1):
                    beta[t][i] = 0
            else:
                for i in range(1, N+1):
                    a = max([self.logps[1][i][j] + beta[t+1][j] + self.logps[2][j][O[t+1]] for j in range(1, N+1)])
                    beta[t][i] = a + log(sum([e ** (self.logps[1][i][j] + beta[t+1][j] + self.logps[2][j][O[t+1]] - a) for j in range(1, N+1)]))
        return beta
        

    def backward_probability(self, beta, sequence):
        O = sequence
        N = len(self.logps[0])
        a = max([self.logps[0][i] + self.logps[2][i][O[0]] + beta[0][i] for i in range(1, N+1)])
        return a + log(sum([e ** (self.logps[0][i] + self.logps[2][i][O[0]] + beta[0][i] - a) for i in range(1, N+1)]))

    def forward_backward(self, sequence):
        O = sequence
        T = len(sequence)
        N = len(self.logps[0])
        alpha = self.forward(sequence)
        beta = self.backward(sequence)
        new_logps = (dict(), {i:dict() for i in range(1, N+1)}, {i:dict() for i in range(1, N+1)})
        E = [self.xi_matrix(t, sequence, alpha, beta) for t in range(T-1)]
        gamma = [dict() for t in range(T)]
        for t in range(T):
            if t < T-1:
                for i in range(1, N+1):
                    a = max([E[t][i][j] for j in range(1, N+1)])
                    gamma[t][i] = a + log(sum([e ** (E[t][i][j] - a) for j in range(1, N+1)]))
            else:
                for i in range(1, N+1):
                    a = max([alpha[t][x] + beta[t][x] for x in range(1, N+1)])
                    gamma[t][i] = alpha[t][i] + beta[t][i] - (a + log(sum([e ** (alpha[t][x] + beta[t][x] - a) for x in range(1, N+1)])))
        for i in range(1, N+1):
            new_logps[0][i] = gamma[0][i]
        for i in range(1, N+1):
            for j in range(1, N+1):
                a_E = max([E[t][i][j] for t in range(T-1)])
                a_gamma = max([gamma[t][i] for t in range(T-1)])
                new_logps[1][i][j] = a_E + log(sum([e ** (E[t][i][j] - a_E) for t in range(T-1)])) - (a_gamma + log(sum([e ** (gamma[t][i] - a_gamma) for t in range(T-1)])))
        for i in range(1, N+1):
            for k in self.logps[2][i]:
                a1 = max([(-sys.float_info.max if O[t] != k else 0) + gamma[t][i] for t in range(T)])
                a_gamma = max([gamma[t][i] for t in range(T)])
                new_logps[2][i][k] = a1 + log(sum([e ** ((-sys.float_info.max if O[t] != k else 0) + gamma[t][i] - a1) for t in range(T)])) - (a_gamma + log(sum([e ** (gamma[t][i] - a_gamma) for t in range(T)])))
        return new_logps        
        
        

    def xi_matrix(self, t, sequence, alpha, beta):
        O = sequence
        N = len(self.logps[0])
        Et = {i:dict() for i in range(1, N+1)}
        a = max([alpha[t][i] + self.logps[1][i][j] + self.logps[2][j][O[t+1]] + beta[t+1][j] for i in range(1, N+1) for j in range(1, N+1)])
        for i in range(1, N+1):
            for j in range(1, N+1):
                Et[i][j] = alpha[t][i] + self.logps[1][i][j] + self.logps[2][j][O[t+1]] + beta[t+1][j] - (a + log(sum([e ** (alpha[t][x] + self.logps[1][x][y] + self.logps[2][y][O[t+1]] + beta[t+1][y] - a) for x in range(1, N+1) for y in range(1, N+1)])))
        return Et                

    def update(self, sequence, cutoff_value):
        p_last = self.forward_probability(self.forward(sequence))
        new_logps = self.forward_backward(sequence)
        new_h = HMM(new_logps)
        p_current = new_h.forward_probability(new_h.forward(sequence))
        while(p_current - p_last >= cutoff_value):
            self.logps = new_logps
            p_last = p_current
            new_logps = self.forward_backward(sequence)
            new_h = HMM(new_logps)
            p_current = new_h.forward_probability(new_h.forward(sequence))

if __name__ == '__main__':
    s = load_corpus('data\\Brown_sample.txt')
    print len(s)
    print s[1208:1228]
    print '1.1', len(s) == 2824, s[1208:1228] == "to the best interest"


    p = load_probabilities('data\\simple.pickle')
    result1 = p[1][1]
    print result1
    p = load_probabilities('data\\prob_vector.pickle')
    result2 = p[1][1]
    print result2
    print '1.2', result1 == {1: -0.6931471805599453, 2: -0.6931471805599453}, result2 == {1: -0.9394332049935226, 2: -1.663989250166688, 3: -2.563281364593492, 4: -1.070849586518945}

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    f = h.forward(s)
    result1 = f[10]
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    f = h.forward(s)
    result2 = f[1400]
    print result2
    print '1.3', result1 == {1: -22.0981588201684, 2: -22.0981588201684}, result2 == {1: -4570.10024680558, 2: -4569.896509256886, 3: -4569.956231590213, 4: -4569.542222483007}

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    result1 = h.forward_probability(h.forward(s))
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    result2 = h.forward_probability(h.forward(s))
    print result2
    print '1.4', result1 == -36.972292832050975, result2 == -9201.34957430782

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    b = h.backward(s)
    result1 = b[9]
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    b = h.backward(s)
    result2 = b[1424]
    print result2
    print '1.5', result1 == {1: -17.513191341497826, 2: -17.513191341497826}, result2 == {1: -4553.117090965298, 2: -4553.249309905892, 3: -4553.085375790753, 4: -4553.140279571696}

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    result1 = h.backward_probability(h.backward(s), s)
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    result2 = h.backward_probability(h.backward(s), s)
    print result2
    print '1.6', result1 == -36.97229283205097, result2 == -9201.349574307758

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    result1 = h.xi_matrix(5, s, h.forward(s), h.backward(s))[2]
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    result2 = h.xi_matrix(500, s, h.forward(s), h.backward(s))[1]
    print result2
    print '1.7', result1 == {1: -1.3862943611198943, 2: -1.3862943611198943}, result2 == {1: -2.5704875729134073, 2: -3.418873166145204, 3: -3.8974061320204783, 4: -2.080487933135373}

    s = "the cat ate the rat"
    p = load_probabilities('data\\simple.pickle')
    h = HMM(p)
    p2 = h.forward_backward(s)
    h2 = HMM(p2)
    result1 = h2.forward_probability(h2.forward(s))
    print result1
    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    p2 = h.forward_backward(s)
    h2 = HMM(p2)
    result2 = h2.forward_probability(h2.forward(s))
    print result2
    print '1.8', result1 == -34.37400550438377, result2 == -8070.961574771892

    s = load_corpus('data\\Brown_sample.txt')
    p = load_probabilities('data\\prob_vector.pickle')
    h = HMM(p)
    h.update(s, 1)
    result = h.forward_probability(h.forward(s))
    print result
    print '1.9', result == -7384.200943071948

